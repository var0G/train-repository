## Install queriments
- In order to run this you need to have python==3.10.5
- do `virtualenv env` in the cloned folder
- activate the envoriment with `source env/bin/activate`
- Install requiments with `pip3 install -r requirements.txt`
- Download the dataset with `python3 get_dataset.py` inside this repository and
  `unzip dataset.zip`
- Change the path on the file fruits.yaml to your absolute path where the folder dataset is
Example:
```yaml
path: /home/user/this-repository/dataset
```
- Run  the `train.py` file with `python3 train.py`
- To run the test `python3 test.py`

## Demo docker App
This demo is just for test the front and backend without any docker build.
- `docker-compose up -d`
