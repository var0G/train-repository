from yolov5 import detect, val

# Run the test on the trained model
val.run(imgsz=640, task="test", data='fruits.yaml', weights='runs_fruits/train_fruits/wights/best.pt')
