from yolov5 import train
# Train with image size 640, batch size 8, 140 epochs and pretrained model
# yolov5m  
train.run(imgsz=640, batch_size=8, epochs=140, data='fruits.yaml', weights='yolov5m.pt', cache='ram', project='runs_fruits', name='train_fruits')
